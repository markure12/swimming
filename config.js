// export const  ip = "10.31.3.17:8080"
// export const ip = "localhost:8080"
export const ip = "123.56.75.160:80"//阿里云
 // export const ip = "192.168.0.106:8080"//子易本地
export const BASE_URL = "http://" + ip + "/api"
// export const BASE_URL = "http://192.168.0.106:8080/api"
// export const  BASE_URL = "http://123.56.75.160:80/api"

//获取请求头
export function getHeader(contentType) {
    let header = {}
    if (contentType == undefined) {
        header = {
            "content-type": "application/x-www-form-urlencoded"
        }
    } else {
        header = {
            "content-type": contentType
        }
    }
    let tokenValue = uni.getStorageSync('satoken'); // 从本地缓存读取tokenValue值
    if (tokenValue != undefined && tokenValue != '') {
        header['satoken'] = tokenValue;
    }
    return header;
}
