import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store=new Vuex.Store({
    state:{
        SocketState:{},
        SocketStateErr:{},
        UserInfo:{},//包括userType
    },
    mutations:{
        setSocketState(that,info){
            that.SocketState=info
        },
        setSocketStateErr(that,info){
            that.SocketStateErr=info;
        },
        setUserInfo(that,info){
            that.UserInfo=info;
        }
    }
})
export default store;
