import Vue from 'vue'
import socket from "./socket.js"; //引入socket.js 重要
import {ip} from "../../config";
import store from "./store.js";

const Socket = new socket({
    url: 'ws://' + ip + '/chat', //连接地址 必填
    maxInterValCount: 5,
    interValTime: 2000,
    onOpen(res) {
        console.log('连接成功')
        let msg = "ping"
        this.nsend(msg)
    },
    onClose(err) {
        uni.showToast({
            title: "连接异常",
            icon: "error"
        })
        console.log('关闭了连接')
    },
    onReload(res) {
        console.log('重载：' + res)
    },
    onRdFinsh(count) {
        console.log(count + '次重连已完成')
    },
    onMsg(msg) {
        const res = JSON.parse(msg.data);
        console.log(res)
        store.commit('setSocketState', res)
    }
})
Vue.prototype.$Socket = Socket;
