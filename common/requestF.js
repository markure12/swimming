export default class Request {
    http (router,data,method="GET") {
        // 基础地址
        let path = 'http://api.meitutu.kunkeji.com/IntegrateApi/';
        // let path = 'http://api.meitutu.kunkeji.com/';
        // 返回promise
        return new Promise((resolve,reject) => {
            // 请求
            uni.request({
                url: `${path}${router}`,
                data: data,
                method:method,
				  header:{
				      'content-type':'application/x-www-form-urlencoded',
				    },
                success: (res) => {
                    // 将结果抛出
                    resolve(res.data)
                }
            })
        })    
    }
}