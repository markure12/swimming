import {BASE_URL, getHeader} from "config.js"


// 学员更新个人信息 course_id user_id  !!!!!!!无用
export const createAppointment = (data) => {
    return new Promise((resolve, reject) => {
        uni.request({
            url: BASE_URL + '/appointment',
            data: {
                "appointment_time": "2021-05-17 12:00,2021-05-17 12:00",
                "course_id": 178,
                "trade_amount": 0.01,
                "trade_name": "测试订单名称",
                "user_id": 131,
                "description": "测试订单"
            },
            method: 'PUT',
            header: getHeader(),
            success: (res) => {
                resolve(res)
            },
            error: (res) => {
                reject(res)
            }
        });
    })
}


// 根据user_id获取预约信息
export const getAppointmentById = (data) =>{
    return new Promise((resolve, reject) => {
        uni.request({
            url: BASE_URL + '/appointment',
            data: data,
            method: 'GET',
            header: getHeader(),
            success: (res) => {
                resolve(res)
            },
            error: (res) => {
                reject(res)
            }
        });
    })
}
