import {BASE_URL, getHeader} from "config.js"
//获取聊天历史记录
export const getHistory = (data) => {
    return new Promise((resolve, reject) => {
        uni.request({
            url: BASE_URL + '/message/getHistory',
            method: 'GET',
            data:data,
            header: getHeader(),
            success: (res) => {
                resolve(res)
            },
            error: (res) => {
                reject(res)
            }
        });
    })
}


// 设置已读
export const setRead = (data) => {
    return new Promise((resolve, reject) => {
        uni.request({
            url: BASE_URL + '/message/setRead',
            method: 'POST',
            data: data,
            header: getHeader(),
            success: (res) => {
                resolve(res)
            },
            error: (res) => {
                reject(res)
            }
        })
    })
}
