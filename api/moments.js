import {BASE_URL, getHeader} from "config.js"


//发布动态
export const publishMoment = (formData) => {
    return new Promise((resolve, reject) => {
        uni.request({
            url: BASE_URL + '/moment',
            method: 'POST',
            data:formData,
            header: getHeader(),
            success: (res) => {
                resolve(res)
            },
            error: (res) => {
                reject(res)
            }
        });
    })
}
//获取动态信息
export const getMoments = (options) => {
	let url = BASE_URL + "/moment";
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'GET',
			data:options,
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: err,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//点赞
export const like = (options) => {
	let url = BASE_URL + "/like";
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'POST',
			data:options,
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: err,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//取消点赞
export const cancel = (options) => {
	let url = BASE_URL + "/like";
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'DELETE',
			data:options,
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: err,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//删除动态
export const delete_moment = (momentId) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/moment/' + momentId,
			method: 'DELETE',
			header: getHeader(),
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}