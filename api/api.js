
import {BASE_URL,getHeader} from "config.js"
export const BASEURL = BASE_URL

//获取标签
export const getLabels = (options) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + "/labels",
			method: 'GET',
			header: {
				"content-type": "application/x-www-form-urlencoded"
			},
			success: (res) => {
				if (res.data.code == 200) {
					let return_data = res.data.data;
					resolve(return_data);
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}

//登录
export const login = (options) => {
	let url = '';
	if (options.type == 'user') {
		url = BASE_URL + "/users/login";
	} else if (options.type == 'trainer') {
		url = BASE_URL + "/trainers/login";
	}
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'POST',
			header: getHeader(),
			data: options.data,
			success: (res) => {
				console.log(res)
				uni.setStorageSync('satoken', res.data.data.satoken);
				if (res.data.code == 200) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					uni.switchTab({
						url: "../index/index"
					})
					return;
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
				}
				resolve(res)
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}

//获取用户信息
export const getInfo = (options) => {
	let url = BASE_URL + "/getInfo";
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'GET',
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: err,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}

//更新头像
export const updateAvatar = (type, imgFiles, userInfo) => {
	let url = '';
	if (type == 'user') {
		url = BASE_URL + "/users/uploadAvatar";
	} else {
		url = BASE_URL + "/trainers/uploadAvatar";
	}
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: url, //后端用于处理图片并返回图片地址的接口
			filePath: imgFiles[0],
			name: 'file',
			formData: {
				id: userInfo.id
			},
			method: "POST",
			success: (res) => {
				let return_data = JSON.parse(res.data);
				resolve(return_data);
				// data1 = return_data.data;
				// localStorage.setItem('userd', JSON.stringify(return_data.data));
			},
			fail: (err) => {
				reject(err);
			}
		})
	})
	return task;


}

//用户注册
export const register = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + "/users",
			method: 'POST',
			data: data,
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			success: (res) => {
				console.log("注册res：")
				console.log(res)

				resolve(res)
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}

//学员更新个人信息
export const updateUserInfo = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/users',
			data: data,
			method: 'PUT',
			header: getHeader(),
			success: (res) => {
				resolve(res)

			},
			error: (res) => {
				reject(res)
			}
		});
	})
}


//根据课程id   获取课程信息
export const getCourseInfoById = (courseId) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses/' + courseId,
			method: 'GET',
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res.data.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}

//删除课程（配合编辑课程使用，先删除再添加）
export const deleteCourseById = (courseId) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses/' + courseId,
			method: 'DELETE',
			header: getHeader(),
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}

//创建课程 文字部分
export const createCourse = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses/text',
			method: 'POST',
			header: getHeader(),
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//更新课程 文字部分
export const updateCourse = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses/text',
			method: 'PUT',
			header: getHeader(),
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//更新订单信息部分（待上课变为已完成）
export const updateAppointment = (id) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url:BASEURL + '/appointment/' + id,
			method: 'PUT',
			header: getHeader(),
			data: {
				id:id,
				is_completed:ture,
			},
			success: (res) => {
				console.log('========================')
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//清空课程的图片信息
export const clearCourseImgs = (courseId,index) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses/clearPictures',
			method: 'POST',
			header: getHeader(),
			data: {
				courseId: courseId,
				index:index,
			},
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}


//获取当前教练下的所有课程
export const getAllCourse = (options) =>{
	return new Promise((resolve, reject) => {
		console.log(options);
		uni.request({
			url: BASE_URL+"/courses/",
			method: 'GET',
			header: {"content-type": "application/x-www-form-urlencoded" },
			data :options,
			success: (res) => {
				console.log(res);
				if (res.data.code == 200) {
					let return_data = res.data.data;
					resolve(return_data);
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}


//退出登录
export const Logout = () => {
	let type = uni.getStorageSync("usertype")
	let url
	if(type=="trainer"){
		url = "/trainers/logout"
	}else if(type=="user"){
		url = "/users/logout"
	}
	console.log(url)
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL+url,
			method: 'GET',
			header: getHeader(),
			success: (res) => {
				console.log(res);
				if (res.data.code == 200) {
					resolve(res);
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg,
						icon: "none"
					})
					resolve(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//获取课程
export const getCourse= (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/courses',
			method: 'GET',
			header: getHeader(),
			data: data,
			success: (res) => {
				if(res.data.code==200){
					resolve(res.data)
				}else if (res.data.code == 400) {
					resolve(res.data)
				}else{
					reject(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//获得所有用户
export const getUsers= (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/users',
			method: 'GET',
			header: getHeader(),
			data: data,
			success: (res) => {
				if(res.data.code==200){
					resolve(res.data)
				}else if (res.data.code == 400) {
					resolve(res.data)
				}else{
					reject(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//获得所有教练
export const getTrainers= (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/trainers',
			method: 'GET',
			header: getHeader(),
			data: data,
			success: (res) => {
				if(res.data.code==200){
					resolve(res.data)
				}else if (res.data.code == 400) {
					resolve(res.data)
				}else{
					reject(res)
				}
			},
			fail: (err) => {
				uni.showToast({
					title: res,
					icon: "none"
				})
				reject(err)
			}
		})
	})
}
//创建订单
export const createIndent = (data) => {
	console.log(data);
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/appointment',
			method: 'PUT',
			header: getHeader(),
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//开通会员
export const becomeVIP = (data) => {
	console.log(data);
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/users/becomeVIP',
			method: 'GET',
			header: getHeader(),
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//根据id获取用户信息
export const getUserInfoById = (user_id) =>{
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/users/'+user_id,
			method: 'GET',
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
//根据id获取教练信息
export const getTrainerInfoById = (user_id) =>{
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + '/trainers/'+user_id,
			method: 'GET',
			header: getHeader(),
			success: (res) => {
				if (res.data.code == 200) {
					resolve(res.data)
				} else if (res.data.code == 400) {
					uni.showToast({
						title: res.data.msg
					})
					resolve(res.data)
				}
				reject(res)
			},
			error: (res) => {
				reject(res)
			}
		});
	})
}
