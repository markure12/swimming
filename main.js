import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
import sunUiBasic from './components/sunui-upimg/sunui-upimg-basic.vue'

import store from "./common/chat/store";
import './common/chat/userSocket.js'
Vue.prototype.$store = store

Vue.use(uView);
Vue.component('sunui-upbasic',sunUiBasic)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    store,
    ...App
})
app.$mount()
